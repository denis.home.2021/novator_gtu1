import flask 
from flask import render_template 
import pickle 
import sklearn 
import numpy as np

app = flask.Flask(__name__, template_folder='templates') 

@app.route('/', methods=['POST', 'GET']) 
@app.route('/index', methods=['POST', 'GET']) 

def main():
    if flask.request.method == 'GET':
        return render_template('main.html') 

    if flask.request.method == 'POST':
        with open('gb_model.pkl', 'rb') as f:
            loaded_model = pickle.load(f)

        test_data = []
        X1 = float(flask.request.form['X1']) 
        X2 = float(flask.request.form['X2']) 
        X4 = float(flask.request.form['X4']) 
        X11 = float(flask.request.form['X11'])
        X15 = float(flask.request.form['X15'])
        X16 = float(flask.request.form['X16'])
        X17 = float(flask.request.form['X17'])
        X32 = float(flask.request.form['X32'])
        X39 = float(flask.request.form['X39'])        
        test_data.append(X1)
        test_data.append(X2)
        test_data.append(X4)
        test_data.append(X11)
        test_data.append(X15)
        test_data.append(X16)
        test_data.append(X17)
        test_data.append(X32)
        test_data.append(X39)
        test_data = np. array([test_data])

        y_pred = loaded_model.predict(test_data) 
        depth = round(y_pred[0]/1000, 2)
        

        return render_template('main.html', 
            result=str(depth)
        ) 

if __name__ == '__main__':
    app.run()